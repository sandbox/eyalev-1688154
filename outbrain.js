/*
 * JavaScript for Outbrain module.
 */

var select_element  = "outbrain_select_lang";
var select_div    = "outbrain_select_div";
var translator_div  = "outbrain_translator";
var form_element  = "outbrain-admin-settings";
var claim_element = "edit-outbrain-claim-key";
var currKeyCode   = '';
var block_settings  = "block_outbrain_settings_link";

// Claiming script variables.
var OBCKey        = '';
var OBCTm       = 0;
var currKeyCode     = '';
var lastClaimedKeyCode  = '';


var outbrain_settingsUrl = 'http://www.outbrain.com/ln/BlogSettings?key=';

var enableAllelements       = new Array('block_language','block_claim','block_pages','block_recommendations','outbrain_select_div','block_outbrain_settings_link','block_submit');
var claimModeElements       = new Array('block_claim','block_pages','block_outbrain_settings_link');
var noClaimModeElements     = new Array('block_language','block_claim','block_pages','outbrain_lang_span');
var claimModeElements_hide    = new Array('block_loader');
var noClaimModeElements_hide  = new Array('block_loader');

// To verify that the return data trigers the page load display.
var isPageLoadMode = false;

function outbrain_$(id){ return document.getElementById(id); }

function get_langs(){
  if (typeof(language_list) == "undefined"){
    setTimeout(get_langs,1000);
  } else {
    var con = '';
    var current = outbrain_$("outbrain-admin-settings").outbrain_translation.value;
    var selectedIdx = -1;
    var defaultIdx = -1;
    var langInfo = null;

    con += '<select id="langs_list" onchange="outbrain_changeLang(language_list[this.selectedIndex]);">';
    for (i = 0; i < language_list.length; i++){
      if (current == language_list[i][1]){
        selectedIdx = i;
      }
      con += "<option value='" + language_list[i][1] + "'>";
      con += language_list[i][0];
      con += "</option>";

      if (language_list[i][1] == current){
        langInfo = language_list[i];
      }

      if ((langInfo == null) && (language_list[i][3] == true)){
          langInfo = language_list[i];
          defaultIdx = i;
      }
    }
    con += '</select>';
    con += '<span id="outbrain_cant_see"><a href="http://www.outbrain.com/new/pages/add_translation.html" target="_blank">Can\'t find your language here?</a></span>'

    outbrain_$(select_element).innerHTML += con;

    if (langInfo != null &&  selectedIdx == -1){
      selectedIdx = defaultIdx;
    }

    outbrain_$("langs_list").options[selectedIdx].selected = true;

    if (langInfo == null){
      langInfo = language_list[0];
    }
    outbrain_changeLang(langInfo);
  }
}

function outbrain_changeLang(langInfo){

  var name = langInfo[0];
  var path = langInfo[1];

  var translator = unescape(langInfo[2]);

  if (translator != ''){
    translator = "translator/s: " + translator;
    outbrain_$(translator_div).innerHTML = translator;
  }else{
    outbrain_$(translator_div).innerHTML = '';
  }

  outbrain_$("outbrain-admin-settings").outbrain_translation.value = path;
}

function moveSecondRecOption(){

  var secondRecOption = outbrain_$("edit-outbrain-rater-recommendations-11");
  if (secondRecOption){
    secondRecOption.parentNode.parentNode.style.paddingLeft = "20px";
  } else {
    setTimeout(secondRecOption,1000);
  }
}


function outbrain_start_langs(){


  if (outbrain_$(form_element) == null){
    setTimeout(outbrain_widget_start,1000);
  } else {

    moveSecondRecOption();

    var newElementGlobal = document.createElement("div");
    newElementGlobal.id = select_div;
    newElementGlobal.className = "form-item outbrain-language-item";
    newElementGlobal.style.display  = "none";
    var fieldsetElement = outbrain_$(form_element).getElementsByTagName("fieldset")[0];

    fieldsetElement.insertBefore(newElementGlobal,fieldsetElement.firstChild);

    var titleElement = document.createElement("span");
    newElementGlobal.id = "outbrain_lang_span";
    titleElement.innerHTML = "Select language:";
    newElementGlobal.appendChild(titleElement);


    var selectElement = document.createElement("span");
    selectElement.id = select_element;
    selectElement.className = "form-select";
    newElementGlobal.appendChild(selectElement);


    var newElement = document.createElement("div");
    newElement.id = translator_div;
    newElement.className = "description";
    newElementGlobal.appendChild(newElement);

    var scriptElement = document.createElement("script");
    scriptElement.src = "http://widgets.outbrain.com/language_list.js";
    document.body.appendChild(scriptElement);

    get_langs();

  }
}

// Fill the url part of a claimed user  - that instruct him to visit Outbrain site.
function outbrain_fillURL(key){

  if (document.getElementById(block_settings) != null && document.getElementById(block_settings) != 'undefined'){
    var block_settings_element = document.getElementById(block_settings);
    var newDescriptionNode = document.createElement('div')
    newDescriptionNode.className  = "form-item";
    newDescriptionNode.id     = "custome_setting_description";
    newDescriptionNode.innerHTML  = "<a href=" + outbrain_settingsUrl + encodeURIComponent(key) + " target='_blank'>Configure outbrain settings...</a>";

    block_settings_element.appendChild(newDescriptionNode);
    return;
  }
  return;
}


function returnedClaimData(status,statusString){
  if (isPageLoadMode && (status == 12 || status == 10)){
    // This blog is claimed show the appropriate.
    outbrain_claimMode();
    // Return to claiming mode.
    isPageLoadMode = false;
  }else if (isPageLoadMode){
    outbrain_noClaimMode();
    isPageLoadMode = false;
  }else{
    var textElement = outbrain_$("claiming_after_comment");
    if (textElement){
      textElement.innerHTML   = statusString;
      textElement.style.display = "block";
    }
  }
}

function claimClicked(key){
  var keyScriptElementId  = "outbrainClaimBlog";
  var element = outbrain_$(keyScriptElementId);
  var rnd = Math.random();
  var claimPath = 'http://odb.outbrain.com/blogutils/Claim.action?key='
    + encodeURIComponent(key)
    + '&type=meta&cbk=returnedClaimData&random=' + rnd;

  if (element){
    element.parentNode.removeChild(element);
  }
  var newSE = document.createElement("script");
  newSE.setAttribute('type','text/javascript');
  newSE.setAttribute('id',keyScriptElementId);
  newSE.setAttribute('src', claimPath);
  var heads = document.getElementsByTagName("head");
  if (heads.length > 0){
    heads[0].insertBefore(newSE, heads[0].firstChild);
  }
  return true;
}

function outbrain_create_claim_button(){
  var inputElement  = outbrain_$("edit-outbrain-claim-key");
  if (inputElement){
    var claimButtom = document.createElement("input");
    claimButtom.id = "claim_button";
    claimButtom.className = "form-submit outbrain-form-send";
    claimButtom.type = "button";
    claimButtom.onclick = onClaimClick;
    claimButtom.value = "Send";
    inputElement.parentNode.insertBefore(claimButtom,inputElement.nextSibling);
  }
}

function onClaimClick(){
  var inputElement  = outbrain_$("edit-outbrain-claim-key");
  if (inputElement){
    if (inputElement.value != ''){
      var key = inputElement.value;
      claimClicked(key);
    }
  }
}

function outbrain_isUserClaim(key){
  isPageLoadMode = true;
  doClaim(key)
}

function outbrain_elementsShowHide(arr,isShow) {
  for (t = 0; t < arr.length; t++){
    var currentElement = document.getElementById(arr[t]);
    if (currentElement != null || currentElement != 'undefined'){
      try{
        whatToDo = (isShow) ?  "block" : "none";
        currentElement.style.display  = whatToDo;
      }
      catch(ex){
      }
    }
  }
}

function outbrain_claimMode(){
  // Array of elments and isShow.
  outbrain_elementsShowHide(claimModeElements_hide,false);
  outbrain_elementsShowHide(claimModeElements,true);
}

function outbrain_noClaimMode(){
  outbrain_elementsShowHide(noClaimModeElements_hide,false);
  outbrain_elementsShowHide(noClaimModeElements,true);
}

// Decide what to show according to result of ajax call of the claiming check.
function outbrain_displayByClaiming(){
  if (typeof(outbrain_$(claim_element)) == 'undefined'){
    // Shouldnt happen - user will se blank page.
    return;
  }
  var keyInDB = outbrain_$(claim_element).value;
  if (keyInDB.length > 0){
    isPageLoadMode = true;
    outbrain_fillURL(keyInDB);
    claimClicked(keyInDB);
  }else{
    outbrain_noClaimMode();
  }
}

function outbrain_widget_start(){
  outbrain_start_langs();
  outbrain_displayByClaiming();
  outbrain_create_claim_button();
}

onload = function(){outbrain_widget_start()};
