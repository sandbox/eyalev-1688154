Integration of Outbrain's service into Drupal.

Outbrain module allows you to place recommendations to related 
content from your website or other websites.

The module requires an active account at outbrain.com.

== Installation ==

[Please note: your site must be visible to the outside world
(or at least Outbrain's servers) in order to get validated.]

* Download and enable the module.

* Register or login at outbrain.com.

* Go to 'Manage Blogs' > 'Add Blog' > Choose the Drupal logo > 
  Enter your site's URL > Check the tiny terms box > Click 'Continue'.

* Go to yoursite.com/admin/config/outbrain and copy your newly generated 
  Outbrain key to the matching field.

* Choose relevant node types.

* Click 'Save configuration'.

* After the page has been reloaded click the 'Send' button near the 
  Outbrain key.

* Go to Outbrain's 'Manage Blogs' page and click 'Check Now!' near your 
  site's address.

* When your site is validated, in Outbrain 'Manage Blogs' page go to the 
  site's 'settings' and adjust your settings as you see fit.
  
* You should start see Outbrain's recommendations in your site. 
  If you don't see recommendations try to generate some activity 
  (like clicking internal nodes links) for Outbrain to start collecting 
  statistics.
